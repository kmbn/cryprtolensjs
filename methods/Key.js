const request = require( 'request' ).defaults({rejectUnauthorized:false});
const helpers = require('../internal/HelperMethods.js');

const requestOptions = { json: true, timeout: 6000, rejectUnauthorized: false, strictSSL: false }

module.exports = class Key {
    
    static Activate(apiToken, rsaPubKey, ProductId, licenseKey, MachineCode = "", FieldsToReturn = 0, Metadata=false, FloatingTimeInterval = 0, MaxOverdraft = 0) {

        return new Promise((resolve, reject) => {
            request(`http://app.cryptolens.io/api/key/Activate?token=${apiToken}&productId=${ProductId}&Key=${licenseKey}&machineCode=${MachineCode}&fieldsToReturn=${FieldsToReturn}&Metadata=${Metadata}&FloatingTimeInterval=${FloatingTimeInterval}&MaxOverdraft=${MaxOverdraft}&Sign=true&SignMethod=1`, requestOptions, (err, res, body) => {
                // if (err || body.result !== 0) {
                //     console.warn(err);
                //     if(!err) {
                //         console.warn(body.message);
                //     }
                //     reje(null);
                if (err) {
                    reject(err)
                } else if (body.result !== 0) {
                    if (body.message) {
                        reject(new Error(body.message))
                    } else {
                        reject(new Error("Received invalid response from license server"))
                    }
                } else {
                    if(helpers.VerifySignature(body, rsaPubKey)) {
                        var license = JSON.parse(Buffer.from(body["licenseKey"], "base64").toString("utf-8"));
                        license.RawResponse = body;
                        resolve(license);
                    } else{
                        console.warn("Signature verification failed.");
                        reject(new Error("Signature verification failed."))
                    }
                }
            });
        });
      
    }

    static GetKey(apiToken, rsaPubKey, ProductId, licenseKey, FieldsToReturn = 0, Metadata=false) {

        return new Promise((resolve, reject) => {
            request(`https://app.cryptolens.io/api/key/GetKey?token=${apiToken}&productId=${ProductId}&Key=${licenseKey}&fieldsToReturn=${FieldsToReturn}&Metadata=${Metadata}&Sign=true&SignMethod=1`, requestOptions, (err, res, body) => {
                if (err || body.result !== 0) {
                    console.warn(err);
                    if(!err) {
                        console.warn(body.message);
                    }
                    resolve(null);
                } else {
                    if(helpers.VerifySignature(body, rsaPubKey)) {
                        var license = JSON.parse(Buffer.from(body["licenseKey"], "base64").toString("utf-8"));
                        license.RawResponse = body;
                        resolve(license);
                    } else{
                        console.warn("Signature verification failed.");
                        resolve(null);
                    }
                }
            });
        });
      
    }

    static deactivate(apiToken, productId, licenseKey, machineCode="", isFloating=false) {

        return new Promise((resolve, reject) => {
            request(`https://app.cryptolens.io/api/key/Deactivate?token=${apiToken}&productId=${productId}&Key=${licenseKey}&MachineCode=${machineCode}&Floating=${isFloating}`, requestOptions, (err, res, body) => {
                if (err || body.result !== 0) {
                    console.warn(err);
                    if(!err) {
                        console.warn(body.message);
                    }
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    }
}
